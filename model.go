package main

import (
	"fmt"
	"math"
	"sync"

	"github.com/omer-s/neuralnet"
)

type ModelTrainer struct {
	mutex          *sync.Mutex
	trainingInput  [][]float64
	trainingOutput [][]float64

	neuralNet neuralnet.NeuralNet
}

type TrainingData struct {
	trainingInput  [][]float64
	trainingOutput [][]float64
}

func newModelTrainer() ModelTrainer {
	trainer := ModelTrainer{
		mutex:          &sync.Mutex{},
		trainingInput:  make([][]float64, 0),
		trainingOutput: make([][]float64, 0)}
	return trainer
}

func (trainer *ModelTrainer) Train(iterations int) {

	maxGoroutines := 10
	guard := make(chan struct{}, maxGoroutines)

	for i := 0; i < iterations; i++ {

		guard <- struct{}{} // would block if guard channel is already filled
		go func(n int) {
			fmt.Println("Starting simulation")
			trainingData := trainer.SimulateGameForTraining()
			trainer.mutex.Lock()
			trainer.trainingInput = append(trainer.trainingInput, trainingData.trainingInput...)
			trainer.trainingOutput = append(trainer.trainingOutput, trainingData.trainingOutput...)
			trainer.mutex.Unlock()
			<-guard
		}(i)

	}
	// fmt.Println(len(trainer.trainingInput), " ", len(trainer.trainingOutput))
	// fmt.Println(trainer.trainingInput)
	// fmt.Println(trainer.trainingOutput)
	trainingData := neuralnet.LabeledData{X: trainer.trainingInput, Y: trainer.trainingOutput}
	trainer.neuralNet = neuralnet.NewNeuralNet(
		[]int{26, 52, 1},
		neuralnet.SigmoidActivator{},
		neuralnet.MeanSquaredErrorCostFunction{},
		3.0)

	trainer.neuralNet.Train(&trainingData, &trainingData, 40, 20)
}

func (trainer *ModelTrainer) Evaluate(iterations int) {

	var mutex sync.Mutex
	whiteWins := 0

	maxGoroutines := 10 // limit concurrent runs
	guard := make(chan struct{}, maxGoroutines)
	var wg = sync.WaitGroup{} // required to block the parent function blocked

	fmt.Println("Starting evaluation")

	for i := 0; i < iterations; i++ {

		guard <- struct{}{} // would block if guard channel is already filled
		wg.Add(1)
		go func(n int) {
			fmt.Println("Starting simulation")

			var player1 Player = &ModelPlayer{PlayerBase: PlayerBase{color: WHITE}, trainer: trainer}
			var player2 Player = &RandomPlayer{PlayerBase: PlayerBase{color: BLACK}}

			gameState := trainer.SimulateGame(&player1, &player2, []GamePlayListener{})
			mutex.Lock()
			if gameState.Winner() == WHITE {
				fmt.Println("White won")
				whiteWins++
			}
			mutex.Unlock()
			<-guard
			wg.Done()
		}(i)
	}

	wg.Wait()
	fmt.Println("Evaluation Result: White: ", whiteWins, " Black: ", iterations-whiteWins, " Ratio: ", float64(whiteWins)/float64(iterations))
}

func (trainer *ModelTrainer) SimulateGameForTraining() TrainingData {
	trainingData := TrainingData{
		trainingInput:  make([][]float64, 0),
		trainingOutput: make([][]float64, 0)}

	var player1 Player = &RandomPlayer{PlayerBase: PlayerBase{color: WHITE}}
	var player2 Player = &RandomPlayer{PlayerBase: PlayerBase{color: BLACK}}

	trainer.SimulateGame(&player1, &player2, []GamePlayListener{&trainingData})

	return trainingData
}

func (trainer *ModelTrainer) SimulateGame(player1 *Player, player2 *Player, listeners []GamePlayListener) GameState {

	players := map[Color]*Player{WHITE: player1, BLACK: player2}

	gameState := NewGameState()
	game := NewGame(&gameState, players)
	game.LoopUntilEnd(listeners)

	return gameState
}

func (trainer *TrainingData) GamePlayed(play *Play, gameState *GameState) {
	trainingInputForPlay := ConvertToTrainingInput(play, gameState)
	trainer.trainingInput = append(trainer.trainingInput, trainingInputForPlay)
}

func ConvertToTrainingInput(play *Play, gameState *GameState) []float64 {
	// TODO consider adding whose turn it is?
	input := make([]float64, 24+2) // one per board location + number of bear offs per color.
	// add each mark on the board to input
	for i := 1; i <= 24; i++ {
		// there are either white or black checkers (one of the values is 0), value for black is negated (similar to output)
		input[i-1] = float64(gameState.Checkers[WHITE][i] + (-1 * gameState.Checkers[BLACK][25-i]))
		// fmt.Println("gameState.Checkers[WHITE][i]: ", gameState.Checkers[WHITE][i])
		// fmt.Println("gameState.Checkers[BLACK][i]: ", gameState.Checkers[BLACK][25-i])
		// fmt.Println("input[i]: ", input[i-1])
	}
	input[24] = float64(gameState.BearOffs[WHITE])
	input[25] = float64(gameState.BearOffs[BLACK]) // no need to negate since there's dedicated input

	return input
}

func (trainer *TrainingData) GameFinished(gameState *GameState) {

	output := float64(0.0)
	if gameState.Winner() == WHITE {
		output = 1.0
	} else {
		output = -1.0
	}

	lastOutputSize := len(trainer.trainingOutput)
	for i := lastOutputSize; i < len(trainer.trainingInput); i++ {
		trainer.trainingOutput = append(trainer.trainingOutput, []float64{output})
		// fmt.Println("Add Output: ", output)
	}
}

type ModelPlayer struct {
	PlayerBase
	trainer *ModelTrainer
}

func (player *ModelPlayer) ChoosePlay(plays *PlaySet, gameState *GameState) *Play {

	picked := 0
	bestScore := math.SmallestNonzeroFloat64
	for i, play := range plays.Plays {

		play.Apply()

		trainingInputForPlay := ConvertToTrainingInput(play, gameState)

		play.Undo()

		testData := neuralnet.LabeledData{X: [][]float64{trainingInputForPlay}}
		score := player.trainer.neuralNet.Predict(&testData)[0][0]
		fmt.Println("Play: ", i, " Score: ", score)

		if score >= bestScore {
			bestScore = score
			picked = i
		}
	}
	fmt.Println("Picked Play: ", picked)
	return plays.Plays[picked]
}
