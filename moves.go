package main

import "fmt"

type Move interface {
	Apply()
	Undo()
}

type MoveBase struct {
	GameState *GameState
	Color     Color
	Target    int

	applied bool
	// once applied; indicate whether this move hit opponent checker
	hitOpponent bool

	doApply func() bool
	doUndo  func()
}

func (move *MoveBase) Apply() {

	if move.applied {
		panic("Cannot apply the move multiple times without undo")
	}

	move.hitOpponent = move.doApply()
	move.applied = true
}

func (move *MoveBase) Undo() {

	if !move.applied {
		panic("Cannot undo the move before apply")
	}

	move.doUndo()
	if move.hitOpponent {
		move.GameState.PutChecker(move.Color.Opponent(), OpponentPoint(move.Target))
		move.GameState.Hits[move.Color.Opponent()]--
		move.hitOpponent = false
	}

	move.applied = false
}

type MoveForwardMove struct {
	MoveBase

	Start int
}

func AttemptNewMoveForwardMove(gameState *GameState, color Color, startPoint int, number int) *MoveForwardMove {

	target := startPoint - number
	legitMove := canPutChecker(gameState, color, target)

	if !legitMove {
		return nil
	}

	move := MoveForwardMove{Start: startPoint}
	move.MoveBase = MoveBase{GameState: gameState, Color: color, Target: target, doApply: move.doApply, doUndo: move.doUndo}
	return &move
}

func canPutChecker(gameState *GameState, color Color, target int) bool {
	switch {
	case target < 1 || target > 24:
		return false
	case gameState.HasChecker(color, target):
		return true
	case gameState.CheckerCount(color.Opponent(), OpponentPoint(target)) <= 1: //point empty or hit case (opposite color with 1 checker)
		return true
	default:
		return false
	}
}

func (move *MoveForwardMove) doApply() bool {

	move.GameState.RemoveChecker(move.Color, move.Start)
	hitOpponent := move.GameState.PutChecker(move.Color, move.Target)
	return hitOpponent
}

func (move *MoveForwardMove) doUndo() {

	move.GameState.RemoveChecker(move.Color, move.Target)
	move.GameState.PutChecker(move.Color, move.Start)
}

func (move *MoveForwardMove) String() string {

	// hitIndicator := ""
	// if move.hitOpponent {
	// 	hitIndicator = "H"
	// }
	return fmt.Sprintf("%v -> %v", quadrantNameForPoint(move.Start, move.Color), quadrantNameForPoint(move.Target, move.Color))
}

type EnterCheckerMove struct {
	MoveBase
}

func AttemptNewEnterCheckerMove(gameState *GameState, color Color, number int) *EnterCheckerMove {

	target := 25 - number
	legitMove := canPutChecker(gameState, color, target)

	if !legitMove {
		return nil
	}

	move := EnterCheckerMove{}
	move.MoveBase = MoveBase{GameState: gameState, Color: color, Target: target, doApply: move.doApply, doUndo: move.doUndo}
	return &move
}

func (move *EnterCheckerMove) doApply() bool {

	hitOpponent := move.GameState.PutChecker(move.Color, move.Target)
	move.GameState.Hits[move.Color]--
	return hitOpponent
}

func (move *EnterCheckerMove) doUndo() {

	move.GameState.RemoveChecker(move.Color, move.Target)
	move.GameState.Hits[move.Color]++
}

func (move *EnterCheckerMove) String() string {
	// hitIndicator := ""
	// if move.hitOpponent {
	// 	hitIndicator = "H"
	// }
	return fmt.Sprintf("Enter at %v", quadrantNameForPoint(move.Target, move.Color))
}

type BearOffMove struct {
	MoveBase
}

func AttemptNewBearOffMove(gameState *GameState, color Color, number int) *BearOffMove {

	target, legitMove := CanBearOff(gameState, color, number)

	if !legitMove {
		return nil
	}

	move := BearOffMove{}
	move.MoveBase = MoveBase{GameState: gameState, Color: color, Target: target, doApply: move.doApply, doUndo: move.doUndo}
	return &move
}

func CanBearOff(gameState *GameState, color Color, number int) (target int, canBearOff bool) {

	if !gameState.AllCheckersInHomeBoard(color) {
		target = -1
		canBearOff = false
		return
	}

	//check any checkers at exact location
	if _, exist := gameState.Checkers[color][number]; exist {
		target = number
		canBearOff = true
		return
	}

	//check if the highest checker is less than number, so highest smaller one can be born off
	for i := 6; i > 0; i-- {
		_, exist := gameState.Checkers[color][i]
		if exist {
			if i < number { //highest is smaller than number, we can bear it off
				target = i
				canBearOff = true
			}
			return
		}
	}
	return
}

func (move *BearOffMove) doApply() bool {
	move.GameState.RemoveChecker(move.Color, move.Target)
	move.GameState.BearOffs[move.Color]++
	return false //bear off never hits opponent checker
}

func (move *BearOffMove) doUndo() {
	move.GameState.PutChecker(move.Color, move.Target)
	move.GameState.BearOffs[move.Color]--
}

func (move *BearOffMove) String() string {
	return fmt.Sprintf("Bear off %v", quadrantNameForPoint(move.Target, move.Color))
}
