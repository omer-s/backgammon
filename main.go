package main

import (
	"bytes"
	"fmt"
	"math"
	"math/rand"
	"strconv"
	"time"
)

const (
	// EMPTY corresponds to no checkers at point
	EMPTY = 0
	// WHITE checker
	WHITE = 1
	// BLACK checker
	BLACK = 2
)

// Color represents the color of the checkers on point
type Color uint8

var colors = [...]string{
	"-",
	"W",
	"B",
}

func (color Color) String() string {
	return colors[color]
}

var colorNames = [...]string{
	"-",
	"WHITE",
	"BLACK",
}

func (color Color) Name() string {
	return colorNames[color]
}

// Opponent returns the opponent's color
func (color Color) Opponent() Color {
	switch color {
	case BLACK:
		return WHITE
	case WHITE:
		return BLACK
	default:
		panic(fmt.Sprint("Invalid color: ", color))
	}
}

// GameState represents state of the gameState
type GameState struct {
	// Points [24]Point
	// point vs count per color
	Checkers map[Color]map[int]int
	Hits     Hits
	BearOffs BearOffs
}

// NewGameState initializes a new gameState
func NewGameState() GameState {

	checkers := map[Color]map[int]int{}
	for _, color := range PlayerColors {
		checkers[color] = map[int]int{
			6:  5,
			8:  3,
			13: 5,
			24: 2,
		}
	}

	//setup for testing bear off process
	// checkers[WHITE] = map[int]int{
	// 	2:  3,
	// 	3:  3,
	// 	5:  2,
	// 	6:  5,
	// 	11: 1,
	// 	13: 1,
	// }
	// checkers[BLACK] = map[int]int{
	// 	1:  1,
	// 	4:  1,
	// 	6:  3,
	// 	7:  1,
	// 	13: 1,
	// 	21: 5,
	// 	24: 3,
	// }

	hits := make(map[Color]int)
	bearOffs := make(map[Color]int)

	gameState := GameState{Checkers: checkers, Hits: hits, BearOffs: bearOffs}
	return gameState
}

const CheckersPerPlayer int = 15

var PlayerColors [2]Color = [2]Color{WHITE, BLACK}

func (gameState GameState) Finished() bool {

	if gameState.BearOffs[BLACK] == CheckersPerPlayer ||
		gameState.BearOffs[WHITE] == CheckersPerPlayer {
		return true
	}
	return false
}

func (gameState GameState) Winner() Color {

	switch {
	case gameState.BearOffs[BLACK] == CheckersPerPlayer:
		return BLACK
	case gameState.BearOffs[WHITE] == CheckersPerPlayer:
		return WHITE
	default:
		panic("Game not finished yet")
	}
}

func (gameState GameState) String() string {

	var buffer bytes.Buffer
	buffer.WriteString("\n")

	for level := 1; level <= 15; level++ {

		if level == 1 {
			buffer.WriteString("C ")
			for i := 13; i <= 24; i++ {
				buffer.WriteByte(quadrantNameForPoint(i, Color(WHITE))[1]) //1-6
				if i == 18 {
					buffer.WriteString("  ")
				}
			}
			buffer.WriteString(" D")
			buffer.WriteString("\n")
		}

		buffer.WriteString("  ")

		levelDone := true
		for i := 13; i <= 24; i++ {
			toPrint := "."
			if gameState.Checkers[WHITE][i] >= level {
				toPrint = Color(WHITE).String()
				levelDone = false
			} else if gameState.Checkers[BLACK][OpponentPoint(i)] >= level {
				toPrint = Color(BLACK).String()
				levelDone = false
			}
			buffer.WriteString(toPrint)
			if i == 18 {
				buffer.WriteString("||")
			}
		}
		buffer.WriteString("\n")
		if levelDone { //nothing new, no need to go further
			break
		}
	}

	maxLevel := 0
	for i := 12; i >= 1; i-- {
		maxLevel = int(math.Max(float64(maxLevel), float64(gameState.Checkers[WHITE][i])))
		maxLevel = int(math.Max(float64(maxLevel), float64(gameState.Checkers[BLACK][OpponentPoint(i)])))
	}

	for level := maxLevel; level >= 1; level-- {

		buffer.WriteString("  ")

		levelDone := true
		for i := 12; i >= 1; i-- {
			toPrint := "."
			if gameState.Checkers[WHITE][i] >= level {
				toPrint = Color(WHITE).String()
				levelDone = false
			} else if gameState.Checkers[BLACK][OpponentPoint(i)] >= level {
				toPrint = Color(BLACK).String()
				levelDone = false
			}
			buffer.WriteString(toPrint)
			if i == 7 {
				buffer.WriteString("||")
			}
		}
		buffer.WriteString("\n")

		if level == 1 {
			buffer.WriteString("B ")
			for i := 12; i >= 1; i-- {
				buffer.WriteByte(quadrantNameForPoint(i, Color(WHITE))[1]) //1-6
				if i == 7 {
					buffer.WriteString("  ")
				}
			}
			buffer.WriteString(" A")
		}

		if levelDone { //nothing new, no need to go further
			break
		}
	}

	if gameState.Hits[WHITE] > 0 || gameState.Hits[BLACK] > 0 {
		buffer.WriteString("\nHits: ")
	}
	for _, color := range PlayerColors {
		for i := 0; i < gameState.Hits[color]; i++ {
			buffer.WriteString(color.String())
		}
		if gameState.Hits[color] > 0 {
			buffer.WriteString(" ")
		}
	}

	if gameState.BearOffs[WHITE] > 0 || gameState.BearOffs[BLACK] > 0 {
		buffer.WriteString("\nBear offs: ")
	}
	for _, color := range PlayerColors {
		for i := 0; i < gameState.BearOffs[color]; i++ {
			buffer.WriteString(color.String())
		}
		if gameState.BearOffs[color] > 0 {
			buffer.WriteString(" ")
		}
	}

	return buffer.String()
}

// Point represents the state of a point on board
// type Point struct {
// 	Index int
// 	Color Color
// 	Count int
// }

type Hits map[Color]int
type BearOffs map[Color]int

//{
// HitByColor map[Color]int
// Color Color
// Count int
//}

// Player instance
type Player interface {
	Color() Color
	ChoosePlay(plays *PlaySet, gameState *GameState) *Play
}

// PlayerBase abstract base class for players
type PlayerBase struct {
	color Color
}

// Color is
func (player *PlayerBase) Color() Color {
	return player.color
}

// RandomPlayer plays randomly
type RandomPlayer struct {
	PlayerBase
}

// ChoosePlay plays the turn
func (player *RandomPlayer) ChoosePlay(plays *PlaySet, gameState *GameState) *Play {

	// fmt.Println("RandomPlayer is playing")
	if plays == nil {
		return nil
	}

	playCount := len(plays.Plays)
	if playCount == 0 {
		return nil
	}

	selectedPlay := rand.Intn(playCount)

	// fmt.Printf("Selected play: ")
	// for _, move := range plays.Plays[selectedPlay].moves {
	// 	fmt.Printf("%v ", move)
	// }
	// fmt.Println()

	// var prompt string
	// fmt.Scanf("%s", &prompt)

	return plays.Plays[selectedPlay]
}

type ConsolePlayer struct {
	PlayerBase
}

// ChoosePlay plays the turn
func (player *ConsolePlayer) ChoosePlay(plays *PlaySet, gameState *GameState) *Play {

	// fmt.Println("ConsolePlayer is playing")

	if plays == nil {
		return nil
	}

	//todo plays nil - no option  -- play.moves empty
	playCount := 0
	for i, play := range plays.Plays {

		if len(play.moves) == 0 {
			continue
		}
		if playCount == 0 {
			fmt.Printf("Select a play:\n")
		}
		fmt.Printf("%2d: ", (i + 1))
		for _, move := range play.moves {
			fmt.Printf("%v ", move)
		}
		playCount++
		fmt.Println("")
	}

	if playCount == 0 {
		return nil
	}

	var selectedPlay int
	for _, err := fmt.Scanf("%d", &selectedPlay); err != nil || selectedPlay <= 0 || selectedPlay > playCount; {
		fmt.Println("Invalid selection, enter again: ")
		_, err = fmt.Scanf("%d", &selectedPlay)
	}

	return plays.Plays[selectedPlay-1]
}

// Roll represents the values on dice after casting
type Roll []int

func cast() Roll {

	return Roll{rand.Intn(6) + 1, rand.Intn(6) + 1}
}

// Game is
type Game struct {
	gameState *GameState
	players   map[Color]*Player
}

// NewGame is
func NewGame(gameState *GameState, players map[Color]*Player) Game {
	game := Game{gameState: gameState, players: players}
	return game
}

// MoveOptions is
type PlaySet struct {
	Plays []*Play
}

type Play struct {
	moves []Move
}

func (play *Play) Apply() {
	for _, move := range play.moves {
		move.Apply()
	}
}

func (play *Play) Undo() {
	for i := len(play.moves) - 1; i >= 0; i-- {
		play.moves[i].Undo()
	}
}

func (gameState *GameState) HasHits(color Color) bool {
	return gameState.Hits[color] > 0
}

func (gameState *GameState) HasChecker(color Color, target int) bool {
	_, exists := gameState.Checkers[color][target]
	return exists
}

func (gameState *GameState) CheckerCount(color Color, target int) int {
	count := gameState.Checkers[color][target]
	return count
}

func (gameState *GameState) AllCheckersInHomeBoard(color Color) bool {
	AssertValidColor(color)

	for point := range gameState.Checkers[color] {
		if point > 6 {
			return false
		}
	}
	return true
}

func AssertValidColor(color Color) {
	if color != WHITE && color != BLACK {
		panic(fmt.Sprintf("Unexpected color: %s", color.Name()))
	}
}

func (gameState *GameState) GetPointsWithColor(color Color) []int {
	AssertValidColor(color)

	points := []int{}
	for point := range gameState.Checkers[color] {
		points = append(points, point)
	}
	// fmt.Println("Points with color ", color, points)
	return points
}

func quadrantNameForPoint(point int, color Color) string {

	if color == BLACK {
		point = OpponentPoint(point)
	}

	var quadrantIndex string
	if point <= 12 {
		quadrantIndex = strconv.Itoa(((point - 1) % 6) + 1)
	} else {
		quadrantIndex = strconv.Itoa((6 - ((point - 1) % 6)))
	}

	switch {
	case point >= 1 && point <= 6:
		return "A" + quadrantIndex
	case point <= 12:
		return "B" + quadrantIndex
	case point <= 18:
		return "C" + quadrantIndex
	case point <= 24:
		return "D" + quadrantIndex
	default:
		panic(fmt.Sprint("Invalid index for point ", point))
	}
}

func (gameState *GameState) PutChecker(color Color, target int) (hitOpponent bool) {

	if _, exists := gameState.Checkers[color][target]; exists {
		gameState.Checkers[color][target]++
	} else {
		//todo define struct Point: color+int, with a hash of both and index checkers as map[point]int to simplyfy. Point.opponent will revert both as below.
		if opponentCheckers, exists := gameState.Checkers[color.Opponent()][OpponentPoint(target)]; exists {
			if opponentCheckers > 1 {
				panic(fmt.Sprint("Illegal to enter to point with multiple opponent checkers at ", target))
			}
			delete(gameState.Checkers[color.Opponent()], OpponentPoint(target))
			hitOpponent = true
		}
		gameState.Checkers[color][target] = 1
	}
	if hitOpponent {
		gameState.Hits[color.Opponent()]++
	}
	return
}

func (gameState *GameState) RemoveChecker(color Color, target int) {

	if _, exists := gameState.Checkers[color][target]; !exists {
		panic(fmt.Sprintf("No checkers to remove of color %v at %v", color, target))
	}

	gameState.Checkers[color][target]--
	if gameState.Checkers[color][target] == 0 {
		delete(gameState.Checkers[color], target)
	}
}

func (game *Game) generatePlays(roll Roll, color Color) *PlaySet {

	var rolls []Roll

	if roll[0] != roll[1] { // permutate numbers
		reverse := Roll{roll[1], roll[0]}
		rolls = []Roll{roll, reverse}
	} else { // doubles case - single permutation with 4 moves of identical numbers
		roll = append(roll, roll[0], roll[0])
		rolls = []Roll{roll}
	}

	plays := PlaySet{}
	for _, roll := range rolls {
		game.doGeneratePlays(&plays, roll, color, &Play{})
	}

	removeShorterPlays(&plays)

	//todo de-dupe/index plays

	return &plays
}

func removeShorterPlays(plays *PlaySet) {

	maxMoves := 0
	for _, play := range plays.Plays {
		if maxMoves < len(play.moves) {
			maxMoves = len(play.moves)
		}
	}

	validIndex := 0
	for i, play := range plays.Plays {
		if len(play.moves) == maxMoves {
			plays.Plays[validIndex] = plays.Plays[i]
			validIndex++
		} else {
			// fmt.Printf("Skipping play with less moves (%v vs %v), moves:%v\n", len(play.moves), maxMoves, play.moves)
		}
	}
	plays.Plays = plays.Plays[:validIndex]
}

func OpponentPoint(point int) int {
	return (24 - point) + 1
}

func (game *Game) doGeneratePlays(plays *PlaySet, roll Roll, color Color, currentPlay *Play) {

	if len(roll) == 0 {
		if len(currentPlay.moves) > 0 {
			plays.Plays = append(plays.Plays, currentPlay)
		}
		return
	}

	moves := game.generateMoves(roll[0], color)

	if len(moves) == 0 { //no moves for this number, try any moves remaining
		game.doGeneratePlays(plays, roll[1:], color, currentPlay)
		return
	}

	for _, move := range moves {
		newPlay := Play{moves: make([]Move, len(currentPlay.moves))}
		copy(newPlay.moves, currentPlay.moves)
		newPlay.moves = append(newPlay.moves, move)
		// fmt.Printf("Apply %v\n", move)
		move.Apply()
		game.doGeneratePlays(plays, roll[1:], color, &newPlay)
		// fmt.Printf("Undo %v\n", move)
		move.Undo()
	}
}

// generateMoves generates legit move options for a single move given a number from the roll
func (game *Game) generateMoves(number int, color Color) []Move {

	gameState := game.gameState

	if gameState.HasHits(color) {
		enterCheckerMove := AttemptNewEnterCheckerMove(gameState, color, number)
		if enterCheckerMove == nil {
			return nil
		}
		return []Move{enterCheckerMove}
	}

	moves := []Move{}

	if bearOffMove := AttemptNewBearOffMove(gameState, color, number); bearOffMove != nil {
		moves = append(moves, bearOffMove)
	}

	startPoints := gameState.GetPointsWithColor(color)
	for _, startPoint := range startPoints {
		if moveForwardMove := AttemptNewMoveForwardMove(gameState, color, startPoint, number); moveForwardMove != nil {
			moves = append(moves, moveForwardMove)
		}
	}

	return moves
}

// Player instance
type GamePlayListener interface {
	GamePlayed(play *Play, gameState *GameState)
	GameFinished(gameState *GameState)
}

// Start is
func (game *Game) LoopUntilEnd(listeners []GamePlayListener) {

	var activePlayer Color = WHITE
	for !game.gameState.Finished() {

		fmt.Printf("%v\n\n", game.gameState)
		fmt.Printf("Turn: %v\n", activePlayer.Name())

		roll := cast()

		fmt.Printf("Roll: %v - %v\n\n", roll[0], roll[1])

		plays := game.generatePlays(roll, activePlayer)

		if plays != nil && len(plays.Plays) > 0 {

			player := *game.players[activePlayer]
			play := player.ChoosePlay(plays, game.gameState)

			play.Apply()

			for _, listener := range listeners {
				listener.GamePlayed(play, game.gameState)
			}
		} else {
			fmt.Println("No possible plays, skipping this round")
		}
		activePlayer = activePlayer.Opponent()
	}

	for _, listener := range listeners {
		listener.GameFinished(game.gameState)
	}

	fmt.Printf("%v\n", game.gameState)
	fmt.Printf("\nGame finished!! Winner: %v\n\n", game.gameState.Winner().Name())
}

func PlayGame() {
	gameState := NewGameState()

	// var player1 Player = &ConsolePlayer{PlayerBase: PlayerBase{color: WHITE}}
	var player1 Player = &RandomPlayer{PlayerBase: PlayerBase{color: WHITE}}
	var player2 Player = &RandomPlayer{PlayerBase: PlayerBase{color: BLACK}}
	players := map[Color]*Player{WHITE: &player1, BLACK: &player2}

	game := NewGame(&gameState, players)
	game.LoopUntilEnd([]GamePlayListener{})
}

func main() {

	rand.Seed(time.Now().Unix())
	// PlayGame()

	trainer := newModelTrainer()
	trainer.Train(50000)
	trainer.neuralNet.Serialize("/tmp/backgammon_model_50k")

	// trainer.neuralNet.Activator = neuralnet.SigmoidActivator{}
	// trainer.neuralNet.CostFunction = neuralnet.MeanSquaredErrorCostFunction{}
	// trainer.neuralNet.Deserialize("/tmp/backgammon_model")

	fmt.Println("Starting Evaluation")
	trainer.Evaluate(1000)

	fmt.Println("Starting Demo")
	trainer.Evaluate(1)

	// trainEval := TrainAndEvaluate(RandomPlayer{PlayerBase{mark: X}}, RandomPlayer{PlayerBase{mark: O}}, 50000, &neuralNetTrainer)
	// fmt.Println("Train Eval: ", trainEval)
	// //testEval := Evaluate(StatsPlayer{PlayerBase: PlayerBase{mark: X}, model: xModel}, RandomPlayer{PlayerBase{mark: O}})
	// testEval := Evaluate(NeuralNetPlayer{PlayerBase: PlayerBase{mark: X}, trainer: &neuralNetTrainer}, RandomPlayer{PlayerBase{mark: O}})
	// fmt.Println("Test Eval:  ", testEval)
}
